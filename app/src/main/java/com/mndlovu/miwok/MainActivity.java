package com.mndlovu.miwok;

import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String NUMBERS = "Numbers";
    private static final String FAMILY = "Family Members";
    private static final String COLORS = "Colors";
    private static final String PHRASES = "Phrases";
    private static TextToSpeech textToSpeech = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void moveToAnotherView(View view) {
        TextView textView = (TextView) view;
        String text = textView.getText().toString();
        switch (text) {
            case NUMBERS:
                segue(NumbersActivity.class);
                break;
            case FAMILY:
                segue(FamilyActivity.class);
                break;
            case COLORS:
                segue(ColorsActivity.class);
                break;
            case PHRASES:
                segue(PhrasesActivity.class);
                break;
            default:
                Toast.makeText(this, "none", Toast.LENGTH_LONG).show();
        }
    }

    private void segue(Class<?> cls) {
        Intent i = new Intent(MainActivity.this, cls);
        startActivity(i);
    }

    public static TextToSpeech getTextToSpeech(Context context) {
        if (textToSpeech == null) {
            Log.v("CHECK THIS", "hieght");
            textToSpeech = new TextToSpeech(context , new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        textToSpeech.setLanguage(Locale.UK);
                    }
                }
            });
        }
        return textToSpeech;
    }
}
