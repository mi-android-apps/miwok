package com.mndlovu.miwok;

public class Word {
    private final String defaultTranslation;
    private final String miWokTranslation;

    public Word(String defaultTranslation, String miWokTranslation) {
        this.defaultTranslation = defaultTranslation;
        this.miWokTranslation = miWokTranslation;
    }

    public String getDefaultTranslation() {
        return defaultTranslation;
    }

    public String getMiWokTranslation() {
        return miWokTranslation;
    }
}
