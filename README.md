# Miwok
This app displays lists of vocabulary words for the user to learn the Miwok language.

### Objectives
* Build the structure of the Miwok language app using intents and activities
* Learn to use Arrays, Lists, and Loops to populate the Miwor app with works
* Create a custom class to represent a report cord app
* Integrate images into the app
* Use an external library to add audio to the app

### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "screen one")
![please find images under app-screenshots directory](app-screenshots/screen-2.png "screen tree")
![please find images under app-screenshots directory](app-screenshots/screen-3.png "screen three")
![please find images under app-screenshots directory](app-screenshots/screen-4.png "screen four")
![please find images under app-screenshots directory](app-screenshots/screen-5.png "screen five")
